package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import cs.mad.flashcards.R
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.FlashcardSet

//interface MyCLickListener : View.OnClickListener {
//
//    override fun onClick(v: View?) {
//    }
//
//}

/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recycler_example)

        val foobar = FlashcardSet.getHardcodedFlashcardSets()

        recyclerView?.adapter = FlashcardSetAdapter(foobar)

//        findViewById<Button>(R.id.button).setOnClickListener {
//            // do whatever on click
//        }

        /*
            connect to views using findViewById
            setup views here - recyclerview, button
            don't forget to notify the adapter if the data set is changed
         */

    }
}